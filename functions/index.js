const functions = require('firebase-functions');
const admin = require('firebase-admin');
const express = require('express');
const cors = require('cors');

const app = express();
const customers = express();
const outlet_admins = express();
const drivers = express();
const products = express();
const stocks = express();
const outlets = express();
const transactions = express();
const transaction_details = express();
const payments = express();
const deliveries = express();
const product_discounts = express();
const discounts = express();
const customer_histories = express();
const f_home = express();
const f_order = express();


app.use(cors({ origin: true }));
customers.use(cors({ origin: true }));
outlet_admins.use(cors({ origin: true }));
drivers.use(cors({ origin: true }));
products.use(cors({ origin: true }));
stocks.use(cors({origin:true}));
outlets.use(cors({origin:true}));
transactions.use(cors({origin:true}));
transaction_details.use(cors({origin:true}));
payments.use(cors({origin:true}));
deliveries.use(cors({origin:true}));
product_discounts.use(cors({origin:true}));
discounts.use(cors({origin:true}));
customer_histories.use(cors({origin:true}));
f_home.use(cors({origin:true}));
f_order.use(cors({origin:true}));


app.get('/hello-world', (req, res) => {
  return res.status(200).send('Hello World!');
});

exports.app = functions.https.onRequest(app);
exports.customers = functions.https.onRequest(customers);
exports.outlet_admins = functions.https.onRequest(outlet_admins);
exports.drivers = functions.https.onRequest(drivers);
exports.products = functions.https.onRequest(products);
exports.stocks = functions.https.onRequest(stocks);
exports.outlets = functions.https.onRequest(outlets);
exports.transactions = functions.https.onRequest(transactions);
exports.transaction_details = functions.https.onRequest(transaction_details);
exports.payments = functions.https.onRequest(payments);
exports.deliveries = functions.https.onRequest(deliveries);
exports.product_discounts = functions.https.onRequest(product_discounts);
exports.discounts = functions.https.onRequest(discounts);
exports.customer_histories = functions.https.onRequest(customer_histories);
exports.f_home = functions.https.onRequest(f_home);
exports.f_order = functions.https.onRequest(f_order);

var serviceAccount = require("./permissions.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://fir-api-eb411.firebaseio.com"
});
const db = admin.firestore();


// app
// create
app.post('/api/create', (req, res) => {
	(async () => {
			try {
				await db.collection('items').doc('/' + req.body.id + '/')
						.create({item: req.body.item,category:req.body.category});
				return res.status(200).send();
			} catch (error) {
				console.log(error);
				return res.status(500).send(error);
			}
		})();
});

// read item
app.get('/api/read/:item_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('items').doc(req.params.item_id);
			let item = await document.get();
			let response = item.data();
			return res.status(200).send(response);
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// read all
app.get('/api/read', (req, res) => {
	(async () => {
		try {
			let query = db.collection('items');
			let response = [];
			await query.get().then(querySnapshot => {
				let docs = querySnapshot.docs;
				// eslint-disable-next-line promise/always-return
				for (let doc of docs) {
					const selectedItem = {
						id: doc.id,
						item: doc.data().item
					};
					response.push(selectedItem);
				}
			});
			return res.status(200).send(response);
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// update
app.put('/api/update/:item_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('items').doc(req.params.item_id);
			await document.update({
				item: req.body.item
			});
			return res.status(200).send();
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// delete
app.delete('/api/delete/:item_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('items').doc(req.params.item_id);
			await document.delete();
			return res.status(200).send();
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// customers
// create
customers.post('/api/create', (req, res) => {
	(async () => {
			try {
				await db.collection('customers').add({
            no_hp: req.body.no_hp,
            pin: req.body.pin,
            name: req.body.name,
            email: req.body.email,
            status: req.body.status,
            point: req.body.point,
            born: req.body.born,
            gender: req.body.gender,
            city: req.body.city,
            address: req.body.address,
            created_date: req.body.created_date,
            modified_date:req.body.modified_date
          });
				return res.status(200).send();
			} catch (error) {
				console.log(error);
				return res.status(500).send(error);
			}
		})();
});

// read item
customers.get('/api/read/:customer_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('customers').doc(req.params.customer_id);
			let customer = await document.get();
			let response = customer.data();
			return res.status(200).send(response);
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// read all
customers.get('/api/read', (req, res) => {
	(async () => {
		try {
			let query = db.collection('customers');
			let response = [];
			await query.get().then(querySnapshot => {
				let docs = querySnapshot.docs;
				// eslint-disable-next-line promise/always-return
				for (let doc of docs) {
					const selectedcustomer = {
						customer_id: doc.id,
            no_hp: doc.data().no_hp,
            pin: doc.data().pin,
            name: doc.data().name,
            email: doc.data().email,
            status: doc.data().status,
            point: doc.data().point,
            born: doc.data().born,
            gender: doc.data().gender,
            city: doc.data().city,
            address: doc.data().address,
            created_date: doc.data().created_date,
            modified_date:doc.data().modified_date
					};
					response.push(selectedcustomer);
				}
			});
			return res.status(200).send(response);
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// update
customers.put('/api/update/:customer_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('customers').doc(req.params.customer_id);
			await document.update({
        no_hp: req.body.no_hp,
        pin: req.body.pin,
        name: req.body.name,
        email: req.body.email,
        status: req.body.status,
        point: req.body.point,
        born: req.body.born,
        gender: req.body.gender,
        city: req.body.city,
        address: req.body.address,
        created_date: req.body.created_date,
        modified_date:req.body.modified_date
  });
			return res.status(200).send();
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// delete
customers.delete('/api/delete/:customer_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('customers').doc(req.params.customer_id);
			await document.delete();
			return res.status(200).send();
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// outlet_admins
// create
outlet_admins.post('/api/create', (req, res) => {
	(async () => {
			try {
				await db.collection('outlet_admins').add({
            no_hp: req.body.no_hp,
            pin: req.body.pin,
            name: req.body.name,
            email: req.body.email,
            status: req.body.status,
            point: req.body.point,
            born: req.body.born,
            gender: req.body.gender,
            city: req.body.city,
            address: req.body.address,
            created_date: req.body.created_date,
            modified_date:req.body.modified_date
          });
				return res.status(200).send();
			} catch (error) {
				console.log(error);
				return res.status(500).send(error);
			}
		})();
});

// read item
outlet_admins.get('/api/read/:outlet_admin_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('outlet_admins').doc(req.params.outlet_admin_id);
			let outlet_admin = await document.get();
			let response = outlet_admin.data();
			return res.status(200).send(response);
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// read all
outlet_admins.get('/api/read', (req, res) => {
	(async () => {
		try {
			let query = db.collection('outlet_admins');
			let response = [];
			await query.get().then(querySnapshot => {
				let docs = querySnapshot.docs;
				// eslint-disable-next-line promise/always-return
				for (let doc of docs) {
					const selectedoutlet_admin = {
						outlet_admin_id: doc.id,
            no_hp: doc.data().no_hp,
            pin: doc.data().pin,
            name: doc.data().name,
            email: doc.data().email,
            status: doc.data().status,
            point: doc.data().point,
            born: doc.data().born,
            gender: doc.data().gender,
            city: doc.data().city,
            address: doc.data().address,
            created_date: doc.data().created_date,
            modified_date:doc.data().modified_date
					};
					response.push(selectedoutlet_admin);
				}
			});
			return res.status(200).send(response);
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// update
outlet_admins.put('/api/update/:outlet_admin_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('outlet_admins').doc(req.params.outlet_admin_id);
			await document.update({
        no_hp: req.body.no_hp,
        pin: req.body.pin,
        name: req.body.name,
        email: req.body.email,
        status: req.body.status,
        point: req.body.point,
        born: req.body.born,
        gender: req.body.gender,
        city: req.body.city,
        address: req.body.address,
        created_date: req.body.created_date,
        modified_date:req.body.modified_date
  });
			return res.status(200).send();
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// delete
outlet_admins.delete('/api/delete/:outlet_admin_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('outlet_admins').doc(req.params.outlet_admin_id);
			await document.delete();
			return res.status(200).send();
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// drivers
// create
drivers.post('/api/create', (req, res) => {
	(async () => {
			try {
				await db.collection('drivers').add({
            no_hp: req.body.no_hp,
            pin: req.body.pin,
            name: req.body.name,
            email: req.body.email,
            status: req.body.status,
            point: req.body.point,
            born: req.body.born,
            gender: req.body.gender,
            city: req.body.city,
            address: req.body.address,
            created_date: req.body.created_date,
            modified_date:req.body.modified_date
          });
				return res.status(200).send();
			} catch (error) {
				console.log(error);
				return res.status(500).send(error);
			}
		})();
});

// read item
drivers.get('/api/read/:driver_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('drivers').doc(req.params.driver_id);
			let driver = await document.get();
			let response = driver.data();
			return res.status(200).send(response);
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// read all
drivers.get('/api/read', (req, res) => {
	(async () => {
		try {
			let query = db.collection('drivers');
			let response = [];
			await query.get().then(querySnapshot => {
				let docs = querySnapshot.docs;
				// eslint-disable-next-line promise/always-return
				for (let doc of docs) {
					const selecteddriver = {
						driver_id: doc.id,
            no_hp: doc.data().no_hp,
            pin: doc.data().pin,
            name: doc.data().name,
            email: doc.data().email,
            status: doc.data().status,
            point: doc.data().point,
            born: doc.data().born,
            gender: doc.data().gender,
            city: doc.data().city,
            address: doc.data().address,
            created_date: doc.data().created_date,
            modified_date:doc.data().modified_date
					};
					response.push(selecteddriver);
				}
			});
			return res.status(200).send(response);
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// update
drivers.put('/api/update/:driver_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('drivers').doc(req.params.driver_id);
			await document.update({
        no_hp: req.body.no_hp,
        pin: req.body.pin,
        name: req.body.name,
        email: req.body.email,
        status: req.body.status,
        point: req.body.point,
        born: req.body.born,
        gender: req.body.gender,
        city: req.body.city,
        address: req.body.address,
        created_date: req.body.created_date,
        modified_date:req.body.modified_date
  });
			return res.status(200).send();
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// delete
drivers.delete('/api/delete/:driver_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('drivers').doc(req.params.driver_id);
			await document.delete();
			return res.status(200).send();
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// Products
// create
products.post('/api/create', (req, res) => {
	(async () => {
			try {
				await db.collection('products').add({
            product_name: req.body.product_name,
            category: req.body.category,
            description: req.body.description,
            price:req.body.price
          });
				return res.status(200).send();
			} catch (error) {
				console.log(error);
				return res.status(500).send(error);
			}
		})();
});

// read item
products.get('/api/read/:product_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('products').doc(req.params.product_id);
			let product = await document.get();
			let response = product.data();
			return res.status(200).send(response);
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// read all
products.get('/api/read', (req, res) => {
	(async () => {
		try {
			let query = db.collection('products');
			let response = [];
			await query.get().then(querySnapshot => {
				let docs = querySnapshot.docs;
				// eslint-disable-next-line promise/always-return
				for (let doc of docs) {
					const selectedProduct = {
						product_id: doc.id,
            product_name: doc.data().product_name,
            category: doc.data().category,
            description: doc.data().description,
            price:doc.data().price
					};
					response.push(selectedProduct);
				}
			});
			return res.status(200).send(response);
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// update
products.put('/api/update/:product_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('products').doc(req.params.product_id);
			await document.update({
				product_name: req.body.product_name,
				description: req.body.description,
				price: req.body.price
			});
			return res.status(200).send();
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// delete
products.delete('/api/delete/:product_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('products').doc(req.params.product_id);
			await document.delete();
			return res.status(200).send();
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// stocks
// create
stocks.post('/api/create', (req, res) => {
	(async () => {
			try {
				await db.collection('stocks').add({
            product_id: req.body.product_id,
            outlet_id: req.body.outlet_id,
            total_a_day: req.body.total_a_day,
            rest:req.body.rest,
            created_date:req.body.created_date,
            modified_date:req.body.modified_date
          });
				return res.status(200).send();
			} catch (error) {
				console.log(error);
				return res.status(500).send(error);
			}
		})();
});

// read item
stocks.get('/api/read/:stock_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('stocks').doc(req.params.stock_id);
			let stock = await document.get();
			let response = stock.data();
			return res.status(200).send(response);
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// read all
stocks.get('/api/read', (req, res) => {
	(async () => {
		try {
			let query = db.collection('stocks');
			let response = [];
			await query.get().then(querySnapshot => {
				let docs = querySnapshot.docs;
				// eslint-disable-next-line promise/always-return
				for (let doc of docs) {
					const selectedstock = {
						stock_id: doc.id,
            product_id: doc.data().product_id,
            outlet_id: doc.data().outlet_id,
            total_a_day: doc.data().total_a_day,
            rest:doc.data().rest,
            created_date:doc.data().created_date,
            modified_date:doc.data().modified_date
					};
					response.push(selectedstock);
				}
			});
			return res.status(200).send(response);
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// update
stocks.put('/api/update/:stock_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('stocks').doc(req.params.stock_id);
			await document.update({
        total_a_day: req.body.total_a_day,
        rest:req.body.rest,
        modified_date:req.body.modified_date
			});
			return res.status(200).send();
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// delete
stocks.delete('/api/delete/:stock_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('stocks').doc(req.params.stock_id);
			await document.delete();
			return res.status(200).send();
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// outlets
// create
outlets.post('/api/create', (req, res) => {
	(async () => {
			try {
				await db.collection('outlets').add({
            product_id: req.body.product_id,
            outlet_name: req.body.outlet_name,
            operational_time: req.body.operational_time,
            address:req.body.address,
            map:req.body.map,
            city:req.body.city,
            no_hp:req.body.no_hp
          });
				return res.status(200).send();
			} catch (error) {
				console.log(error);
				return res.status(500).send(error);
			}
		})();
});

// read item
outlets.get('/api/read/:outlet_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('outlets').doc(req.params.outlet_id);
			let outlet = await document.get();
			let response = outlet.data();
			return res.status(200).send(response);
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// read all
outlets.get('/api/read', (req, res) => {
	(async () => {
		try {
			let query = db.collection('outlets');
			let response = [];
			await query.get().then(querySnapshot => {
				let docs = querySnapshot.docs;
				// eslint-disable-next-line promise/always-return
				for (let doc of docs) {
					const selectedoutlet = {
						outlet_id: doc.id,
            product_id: doc.data().product_id,
            outlet_name: doc.data().outlet_name,
            operational_time: doc.data().operational_time,
            address:doc.data().address,
            map:doc.data().map,
            city:doc.data().city,
            no_hp:doc.data().no_hp
					};
					response.push(selectedoutlet);
				}
			});
			return res.status(200).send(response);
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// update
outlets.put('/api/update/:outlet_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('outlets').doc(req.params.outlet_id);
			await document.update({
        outlet_name: req.body.outlet_name,
        operational_time: req.body.operational_time,
        address:req.body.address,
        map:req.body.map,
        city:req.body.city,
        no_hp:req.body.no_hp
			});
			return res.status(200).send();
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// delete
outlets.delete('/api/delete/:outlet_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('outlets').doc(req.params.outlet_id);
			await document.delete();
			return res.status(200).send();
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// transactions
// create
transactions.post('/api/create', (req, res) => {
	(async () => {
			try {
				await db.collection('transactions').add({
            outlet_id: req.body.outlet_id,
            customer_id: req.body.customer_id,
            driver_id: req.body.driver_id,
            transaction_status:req.body.transaction_status,
            address:req.body.address,
            no_hp:req.body.no_hp,
            point:req.body.point,
            created_date:req.body.created_date,
            modified_date:req.body.modified_date
          });
				return res.status(200).send();
			} catch (error) {
				console.log(error);
				return res.status(500).send(error);
			}
		})();
});

// read item
transactions.get('/api/read/:transaction_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('transactions').doc(req.params.transaction_id);
			let transaction = await document.get();
			let response = transaction.data();
			return res.status(200).send(response);
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// read all
transactions.get('/api/read', (req, res) => {
	(async () => {
		try {
			let query = db.collection('transactions');
			let response = [];
			await query.get().then(querySnapshot => {
				let docs = querySnapshot.docs;
				// eslint-disable-next-line promise/always-return
				for (let doc of docs) {
					const selectedtransaction = {
						transaction_id: doc.id,
            outlet_id: doc.data().outlet_id,
            customer_id: doc.data().customer_id,
            driver_id: doc.data().driver_id,
            transaction_status:doc.data().transaction_status,
            address:doc.data().address,
            no_hp:doc.data().no_hp,
            point:doc.data().point,
            created_date:doc.data().created_date,
            modified_date:doc.data().modified_date
					};
					response.push(selectedtransaction);
				}
			});
			return res.status(200).send(response);
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// update
transactions.put('/api/update/:transaction_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('transactions').doc(req.params.transaction_id);
			await document.update({
        transaction_status:req.body.transaction_status,
        address:req.body.address,
        no_hp:req.body.no_hp,
        point:req.body.point,
        modified_date:req.body.modified_date
  });
			return res.status(200).send();
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// delete
transactions.delete('/api/delete/:transaction_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('transactions').doc(req.params.outlet_id);
			await document.delete();
			return res.status(200).send();
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// transaction_details
// create
transaction_details.post('/api/create', (req, res) => {
	(async () => {
			try {
				await db.collection('transaction_details').add({
            transaction_id: req.body.transaction_id,
            product_id: req.body.product_id,
            product_note: req.body.product_note,
            point:req.body.point
          });
				return res.status(200).send();
			} catch (error) {
				console.log(error);
				return res.status(500).send(error);
			}
		})();
});

// read item
transaction_details.get('/api/read/:transaction_detail_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('transaction_details').doc(req.params.transaction_detail_id);
			let transaction_detail = await document.get();
			let response = transaction_detail.data();
			return res.status(200).send(response);
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// read all
transaction_details.get('/api/read', (req, res) => {
	(async () => {
		try {
			let query = db.collection('transaction_details');
			let response = [];
			await query.get().then(querySnapshot => {
				let docs = querySnapshot.docs;
				// eslint-disable-next-line promise/always-return
				for (let doc of docs) {
					const selectedtransaction_detail = {
						transaction_detail_id: doc.id,
            transaction_id: doc.data().transaction_id,
            product_id: doc.data().product_id,
            product_note: doc.data().product_note,
            point:doc.data().point
					};
					response.push(selectedtransaction_detail);
				}
			});
			return res.status(200).send(response);
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// update
transaction_details.put('/api/update/:transaction_detail_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('transaction_details').doc(req.params.transaction_detail_id);
			await document.update({
        product_note: req.body.product_note,
        point:req.body.point
});
			return res.status(200).send();
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// delete
transaction_details.delete('/api/delete/:transaction_detail_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('transaction_details').doc(req.params.transaction_detail_id);
			await document.delete();
			return res.status(200).send();
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// payments
// create
payments.post('/api/create', (req, res) => {
	(async () => {
			try {
				await db.collection('payments').add({
            transaction_id: req.body.transaction_id,
            payment_method: req.body.payment_method,
            payment_status: req.body.payment_status,
            bill: req.body.bill,
            created_date: req.body.created_date,
            modified_date:req.body.modified_date
          });
				return res.status(200).send();
			} catch (error) {
				console.log(error);
				return res.status(500).send(error);
			}
		})();
});

// read item
payments.get('/api/read/:payment_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('payments').doc(req.params.payment_id);
			let payment = await document.get();
			let response = payment.data();
			return res.status(200).send(response);
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// read all
payments.get('/api/read', (req, res) => {
	(async () => {
		try {
			let query = db.collection('payments');
			let response = [];
			await query.get().then(querySnapshot => {
				let docs = querySnapshot.docs;
				// eslint-disable-next-line promise/always-return
				for (let doc of docs) {
					const selectedpayment = {
						payment_id: doc.id,
            transaction_id: doc.data().transaction_id,
            payment_method: doc.data().payment_method,
            payment_status: doc.data().payment_status,
            bill: doc.data().bill,
            created_date: doc.created_date().product_note,
            modified_date: doc.data().modified_date
					};
					response.push(selectedpayment);
				}
			});
			return res.status(200).send(response);
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// update
payments.put('/api/update/:payment_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('payments').doc(req.params.payment_id);
			await document.update({
        payment_status: req.body.payment_status,
        bill: req.body.bill,
        modified_date:req.body.modified_date
});
			return res.status(200).send();
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// delete
payments.delete('/api/delete/:payment_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('payments').doc(req.params.payment_id);
			await document.delete();
			return res.status(200).send();
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// deliveries
// create
deliveries.post('/api/create', (req, res) => {
	(async () => {
			try {
				await db.collection('deliveries').add({
            transaction_id: req.body.transaction_id,
            transaction_detail_id: req.body.transaction_detail_id,
            delivery_status: req.body.delivery_status,
            position: req.body.position,
            created_date: req.body.created_date,
            modified_date:req.body.modified_date
          });
				return res.status(200).send();
			} catch (error) {
				console.log(error);
				return res.status(500).send(error);
			}
		})();
});

// read item
deliveries.get('/api/read/:delivery_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('deliveries').doc(req.params.delivery_id);
			let delivery = await document.get();
			let response = delivery.data();
			return res.status(200).send(response);
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// read all
deliveries.get('/api/read', (req, res) => {
	(async () => {
		try {
			let query = db.collection('deliveries');
			let response = [];
			await query.get().then(querySnapshot => {
				let docs = querySnapshot.docs;
				// eslint-disable-next-line promise/always-return
				for (let doc of docs) {
					const selecteddelivery = {
						delivery_id: doc.id,
            transaction_id: doc.data().transaction_id,
            transaction_detail_id: doc.data().transaction_detail_id,
            delivery_status: doc.data().delivery_status,
            position: doc.data().position,
            created_date: doc.created_date().product_note,
            modified_date: doc.data().modified_date
					};
					response.push(selecteddelivery);
				}
			});
			return res.status(200).send(response);
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// update
deliveries.put('/api/update/:delivery_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('deliveries').doc(req.params.delivery_id);
			await document.update({
        delivery_status: req.body.delivery_status,
        position: req.body.position,
        modified_date:req.body.modified_date
});
			return res.status(200).send();
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// delete
deliveries.delete('/api/delete/:delivery_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('deliveries').doc(req.params.delivery_id);
			await document.delete();
			return res.status(200).send();
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// product_discounts
// create
product_discounts.post('/api/create', (req, res) => {
	(async () => {
			try {
				await db.collection('product_discounts').add({
            product_id: req.body.product_id,
            discount_id: req.body.discount_id,
            description: req.body.description,
            price: req.body.price
          });
				return res.status(200).send();
			} catch (error) {
				console.log(error);
				return res.status(500).send(error);
			}
		})();
});

// read item
product_discounts.get('/api/read/:product_discount_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('product_discounts').doc(req.params.product_discount_id);
			let product_discount = await document.get();
			let response = product_discount.data();
			return res.status(200).send(response);
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// read all
product_discounts.get('/api/read', (req, res) => {
	(async () => {
		try {
			let query = db.collection('product_discounts');
			let response = [];
			await query.get().then(querySnapshot => {
				let docs = querySnapshot.docs;
				// eslint-disable-next-line promise/always-return
				for (let doc of docs) {
					const selectedproduct_discount = {
						product_discount_id: doc.id,
            product_id: doc.data().product_id,
            discount_id: doc.data().discount_id,
            description: doc.data().description,
            price: doc.data().price
					};
					response.push(selectedproduct_discount);
				}
			});
			return res.status(200).send(response);
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// update
product_discounts.put('/api/update/:product_discount_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('product_discounts').doc(req.params.product_discount_id);
			await document.update({
        description: req.body.description,
        price: req.body.price
});
			return res.status(200).send();
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// delete
product_discounts.delete('/api/delete/:product_discount_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('product_discounts').doc(req.params.product_discount_id);
			await document.delete();
			return res.status(200).send();
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// discounts
// create
discounts.post('/api/create', (req, res) => {
	(async () => {
			try {
				await db.collection('discounts').add({
            customer_id: req.body.customer_id,
            converter_point: req.body.converter_point,
            discount_start: req.body.discount_start,
            discount_end: req.body.discount_end
          });
				return res.status(200).send();
			} catch (error) {
				console.log(error);
				return res.status(500).send(error);
			}
		})();
});

// read item
discounts.get('/api/read/:discount_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('discounts').doc(req.params.discount_id);
			let discount = await document.get();
			let response = discount.data();
			return res.status(200).send(response);
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// read all
discounts.get('/api/read', (req, res) => {
	(async () => {
		try {
			let query = db.collection('discounts');
			let response = [];
			await query.get().then(querySnapshot => {
				let docs = querySnapshot.docs;
				// eslint-disable-next-line promise/always-return
				for (let doc of docs) {
					const selecteddiscount = {
						discount_id: doc.id,
            customer_id: doc.data().customer_id,
            converter_point: doc.data().converter_point,
            discount_start: doc.data().discount_start,
            discount_end: doc.discount_end().price
					};
					response.push(selecteddiscount);
				}
			});
			return res.status(200).send(response);
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// update
discounts.put('/api/update/:discount_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('discounts').doc(req.params.discount_id);
			await document.update({
        converter_point: req.body.converter_point,
        discount_start: req.body.discount_start,
        discount_end: req.body.discount_end
});
			return res.status(200).send();
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// delete
discounts.delete('/api/delete/:discount_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('discounts').doc(req.params.discount_id);
			await document.delete();
			return res.status(200).send();
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// customer_histories
// create
customer_histories.post('/api/create', (req, res) => {
	(async () => {
			try {
				await db.collection('customer_histories').add({
            transaction_detail_id: req.body.transaction_detail_id,
            payment_id: req.body.payment_id,
            customer_point: req.body.customer_point,
            outlet_point: req.body.outlet_point,
            driver_point: req.body.driver_point,
            created_date: req.body.created_date,
            modified_date:req.body.modified_date
          });
				return res.status(200).send();
			} catch (error) {
				console.log(error);
				return res.status(500).send(error);
			}
		})();
});

// read item
customer_histories.get('/api/read/:customer_history_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('customer_histories').doc(req.params.customer_history_id);
			let customer_history = await document.get();
			let response = customer_history.data();
			return res.status(200).send(response);
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// read all
customer_histories.get('/api/read', (req, res) => {
	(async () => {
		try {
			let query = db.collection('customer_histories');
			let response = [];
			await query.get().then(querySnapshot => {
				let docs = querySnapshot.docs;
				// eslint-disable-next-line promise/always-return
				for (let doc of docs) {
					const selectedcustomer_history = {
						customer_history_id: doc.id,
            transaction_detail_id: doc.data().transaction_detail_id,
            payment_id: doc.data().payment_id,
            customer_point: doc.data().customer_point,
            outlet_point: doc.data().outlet_point,
            driver_point: doc.data().driver_point,
            created_date: doc.created_date().product_note,
            modified_date: doc.data().modified_date
					};
					response.push(selectedcustomer_history);
				}
			});
			return res.status(200).send(response);
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// update
customer_histories.put('/api/update/:customer_history_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('customer_histories').doc(req.params.customer_history_id);
			await document.update({
        customer_point: req.body.customer_point,
        outlet_point: req.body.outlet_point,
        driver_point: req.body.driver_point,
        modified_date:req.body.modified_date
});
			return res.status(200).send();
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// delete
customer_histories.delete('/api/delete/:customer_history_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('customer_histories').doc(req.params.customer_history_id);
			await document.delete();
			return res.status(200).send();
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// f_homes
// create
f_homes.post('/api/create', (req, res) => {
	(async () => {
			try {
				await db.collection('f_homes').add({
            customer_id: req.body.customer_id,
            status: req.body.status,
            point: req.body.point
          });
				return res.status(200).send();
			} catch (error) {
				console.log(error);
				return res.status(500).send(error);
			}
		})();
});

// read item
f_homes.get('/api/read/:f_home_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('f_homes').doc(req.params.f_home_id);
			let f_home = await document.get();
			let response = f_home.data();
			return res.status(200).send(response);
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// read all
f_homes.get('/api/read', (req, res) => {
	(async () => {
		try {
			let query = db.collection('f_homes');
			let response = [];
			await query.get().then(querySnapshot => {
				let docs = querySnapshot.docs;
				// eslint-disable-next-line promise/always-return
				for (let doc of docs) {
					const selectedf_home = {
						f_home_id: doc.id,
            customer_id: doc.data().customer_id,
            status: doc.data().status,
            point: doc.data().point
					};
					response.push(selectedf_home);
				}
			});
			return res.status(200).send(response);
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// update
f_homes.put('/api/update/:f_home_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('f_homes').doc(req.params.f_home_id);
			await document.update({
        customer_id: req.body.customer_id,
        status: req.body.status,
        point: req.body.point
});
			return res.status(200).send();
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// delete
f_homes.delete('/api/delete/:f_home_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('f_homes').doc(req.params.f_home_id);
			await document.delete();
			return res.status(200).send();
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// f_orders
// create
f_orders.post('/api/create', (req, res) => {
	(async () => {
			try {
				await db.collection('f_orders').add({
            customer_id: req.body.customer_id,
            outlet_id: req.body.outlet_id,
            customer_loc: req.body.customer_loc,
          });
				return res.status(200).send();
			} catch (error) {
				console.log(error);
				return res.status(500).send(error);
			}
		})();
});

// read item
f_orders.get('/api/read/:f_order_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('f_orders').doc(req.params.f_order_id);
			let f_order = await document.get();
			let response = f_order.data();
			return res.status(200).send(response);
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// read all
f_orders.get('/api/read', (req, res) => {
	(async () => {
		try {
			let query = db.collection('f_orders');
			let response = [];
			await query.get().then(querySnapshot => {
				let docs = querySnapshot.docs;
				// eslint-disable-next-line promise/always-return
				for (let doc of docs) {
					const selectedf_order = {
						f_order_id: doc.id,
            customer_id: doc.data().customer_id,
            outlet_id: doc.data().outlet_id,
            outlet_name: doc.data().outlet_name,
            outlet_range: doc.outlet_range().point
					};
					response.push(selectedf_order);
				}
			});
			return res.status(200).send(response);
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// update
f_orders.put('/api/update/:f_order_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('f_orders').doc(req.params.f_order_id);
			await document.update({
        customer_id: req.body.customer_id,
        status: req.body.status,
        point: req.body.point
});
			return res.status(200).send();
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

// delete
f_orders.delete('/api/delete/:f_order_id', (req, res) => {
	(async () => {
		try {
			const document = db.collection('f_orders').doc(req.params.f_order_id);
			await document.delete();
			return res.status(200).send();
		} catch (error) {
			console.log(error);
			return res.status(500).send(error);
		}
	})();
});

